down: ## Stop docker container
	docker-compose stop

install: ## Build Docker images
install: down build

build: ## Build Docker images
	docker-compose build
	docker-compose up -d --remove-orphans

start: ## Start docker images
start: down
	docker-compose up -d --remove-orphans
