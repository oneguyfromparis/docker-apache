# Installation

## Docker
We use docker for local development.
For an optimal experience you should use [Docker environment](https://docs.docker.com/desktop/#download-and-install).

## Install
Copy the .env complete it with your information:

```bash
cp .env.dist .env
```

Then, run this commande ton build Docker container and assets:
```bash
make install
```

## Use

```bash
# To start docker container
make start

# To stop docker container
make down
```

## Assets (if assets)

```bash
# To build assets
make assets

# To build assets and update on change
make watch
```
